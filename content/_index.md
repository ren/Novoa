---
title: Novoa Index
toc: false
---

This is the landing page.

Y esta completamente en construcción pero la publicaré de todas formas.

## Explorar más

{{< cards >}}
  {{< card link="docs" title="Documentación" icon="book-open" >}}
  {{< card link="novoa" title="Acerca de esta servidor" icon="user" >}}
{{< /cards >}}

## Documentación del tema

For more information, visit [Hextra](https://imfing.github.io/hextra).
