---
title: Actualizar Misskey
type: docs
prev: docs/misskey
next: http-server

---

Para actualizar a la versión de misskey más reciente, cualquier versión posterior a la instalación o migrarse a algún fork los comandos generales son:

Para actualizar a la versión más reciente dentro de la rama master, que es la principal y supuestamente estable:

```bash
git checkout master
git pull
git submodule update --init
NODE_ENV=production pnpm install --frozen-lockfile
NODE_ENV=production pnpm run build
pnpm run migrate
```

Para actualizar a un lanzamiento o versión específica de misskey:

`git checkout (git tag -l | grep -v 'rc[0-9]*' | sort -V | tail -n 1)`

o

`git checkout $TAG` por ejemplo `git checkout 2023.12.2`

y despues:

```bash
git pull
git submodule update --init
NODE_ENV=production pnpm install --frozen-lockfile
NODE_ENV=production pnpm run build
pnpm run migrate
```

Ambas métodos de actualización son un poco automatizados en especial en la parte del comando git.

En detalle y la forma correcta y más tardada de hacerlo sería checar primero las notas o cambios (en japonés).

Obtener los cambios en el repositorio en nuestra copia local y situarnos en la rama que queramos usar, ya sea master, develop o alguna versión particular.

```bash
git fetch
git checkout $TAG
```

Posteriormente comparar las diferencias o cambios que se han hecho dentro del código.

```bash
git status
```

Habiendo comparado y estando seguros de que no habrá conflictos vamos a actualizar la rama sobre la que estamos trabajando (la que seleccionamos despues del checkout).

```bash
git merge
```

Y finalmente actualizar misskey, el primer comando para actualizar las dependencias de JavaScript, el segundo para compilar el programa y el tercero para efectuar todos los cambios en la base de datos necesaria para acomodar a la nueva versión.

```bash
NODE_ENV=production pnpm install --frozen-lockfile
NODE_ENV=production pnpm run build
pnpm run migrate
```
