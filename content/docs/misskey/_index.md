---
title: Misskey en Novoa
prev: docs/sysadmin
next: http-server
date: 2024-01-13T13:29:17-06:00
---

Esta guía/documentación esta planteada para una instalación completamente manual sobre algún servidor gnu/linux como Debian de manera directa, es decir no hará uso de kubernetes, docker o cualquier otro software de virtualización, también asume que tienes un acceso a root mediante shell a tu sistema ya sea este un servidor virtual o uno montado en el hardware directamente.

Estoy adaptando un poco la guía que esta ahí fuera sobre misskey porque también fue lo que hice. Cuando inicié esta instancia recuerdo que la única documentación que existía estaba literalmente en chino y en [japonés](https://misskey-hub.net/ja/docs/for-admin/install/guides/manual/) ⬅️ Esa es la guía base y más actualizada sobre como instalar misskey.

## Preparaciones para instalar Misskey

### Crear el usuario

Primero crear la carpeta donde estará alojado todo lo pertinente a nuestra instancia

`sudo mkdir /sys-users/` o cualquier ruta de archivo que se desee.

Posteriormente los [usuarios](/docs/sysadmin/#usuarios-en-linuxunix) deberían ser creados con privilegios mínimos:

```bash
adduser --system --disabled-password --disabled-login --shell=/bin/false --home /sys-users/mk misskey
```

**misskey** al final puede ser cambiado por cualquier otro nombre de usuario.

El comando anterior va a crear una carpeta donde todos los permisos los tiene el usuario recientemente creado en `/sys-users/mk`.

Quizá algunas de esas opciones sean redundantes como `--disabled-login --shell=/bin/false` pues no se puede iniciar sesión sin acceso a shell pero prefiero redundancia y explicidad, además de que no causa ningún problema.

Lo que cada una de estas opciones (*flags* en inglés) marca o configura es que:

- `--system`: Crea un usuario del sistema con un uid < 1000. Usuarios normales tienen un uid > 1000.

- `--disabled-password`: El usuario no puede iniciar sesión usando una contraseña, solo se puede iniciar sesión desde o siendo super usuario o con un usuario con privilegios de super usuario.

- `--disabled-login`: Bloquea el intento de inicio de sesión.

- `--shell=/bin/false`: La opción shell configura la shell por defecto que usara el usuario, al marcar esta opción como falsa el usuario no podrá acceder a ningun entorno de la línea de comandos, es decir no podrá ejecutar ningun comando desde alguna terminal de manera directa.

- `--no-create-home`: el usuario misskey no tenga un directorio dentro de `/home`.

- `--base-dir`*/DIRECTORIO_BASE*: Es donde todos los usuarios serán creados por defecto, y se concatena con el nombre del usuario. Por defecto es `/home` entonces en este caso si no se cambia nada el directorio y variable $HOME del usuario sería `/home/misskey`. Si solo se cambia esta opción y no la que sigue el directorio terminaría siendo algo como `/directorio-distinto/misskey`.

- `--home` */DIRECTORIO_HOME*: En este caso es para más control sobre donde se colocará al usuario. Simplemente hay que poner la ruta completa de la carpeta como `/directorio-distinto/un-nombre-distinto` y ahí es donde se le asiganara el valor de $HOME al usuario, sin embargo hay que tomar en cuenta que el programa no creará ninguna carpeta así que hay que hacerlas directamente con `mkdir`.

### Dependencias del sistema :package:

Dependiendo de que sistema operativo uses los comandos que tengas que utilizar para instalar programas serán distintos, si quieres ayuda con alguno me puedes contactar directamente en XMPP, pues estoy familiarizado con varias versiones de linux.

Antes de instalar las dependencias debes que saber que dependiendo de la versión de misskey (o sus forks) cada una de estos componentes variaran en las versiones mínimas necesarias, por ejemplo cuando yo instalé misskey por primera vez estaba usando postgres 13 y de hecho lo seguía usando incluso cuando ya no era soportado supuestamente y no resultó en ningún problema. Misskey tampoco hace una revisión de las versiones de los programas fuera de node, dicho esto, puede que sea una mejor opción optar por las versiones otorgadas en el repositorio base de tu sistema.

He adjuntado un link a cada uno de los programas necesarios para instalar y ejecturar misskey, quizá en algún futuro haga una documentación sobre como los instale yo en mi sistema ya que cada uno de estos links tiene instrucciones específicas:

- [Node.js](https://github.com/nodesource/distributions) >= 20.4.x ➡️ se usa como la base del servidor, misskey esta construido sobre node.

- [PostgreSQL](https://www.postgresql.org/download/) >= 15.x ➡️ Base de datos donde se almacenarán todos los posts y las relaciones entre usuarios. instancias. etc. de manera permanente.

- [Redis](https://redis.io/docs/install/install-redis/install-redis-on-linux/) >= 7.0 ➡️ Base de datos transaccional usada como caché, para listas y relaciones no permanentes.

- [FFmpeg](https://www.ffmpeg.org/download.html#build-linux) ➡️ Usado para pasar un formato de multimedia a otro.

- En la guía original se menciona instalar `build-essential` para distribuciones basadas en Debian.

- Varias utilidades usadas para el manejo del sistema como git y neo/vim.

Asumiendo que esta usando Debian y agregaste todos esos repositorios al administrador de paquetes (o vas a usar solo los paquetes que provee Debian) tu comando final para instalarlos debería quedar algo como:

```bash
sudo apt install build-essential nodejs postgresql redis ffmpeg
```

### Hacer una base de datos en PostgreSQL

Actualmente misskey no tiene una manera de crear usuarios en postgres automáticamente así que tendremos que hacer uno y establecer su contraseña y métodos de acceso de manera manual.

Primero cambiar al usuario postgres

`su - postgres`

Dentro de ese usuario se creará un usuario exclusivo de misskey para acceder a la base de datos desde el software.

`createuser -P example-misskey-user`

Este comando pedirá una contraseña que debe ser introducida 2 veces.

Finalmente vamos a hacer una base de datos para misskey con el usuario que acabamos de crear.

`createdb -O example-misskey-user misskey-database`

Sin haber nada más que hacer en postgres la manera de salir de la sesión es escribiendo exit, lo cual nos llevara a la sesión anterior.

### Preparar el entorno de compilación

Misskey al ser un progama escrito en JavaScript todas las librerías que usa son en este mismo lenguaje que tiene su propio administrador de paquetes fuera de apt/debian, para esto debemos habilitar `pnpm` y para la compilación y tener las caracteristicas de un servidor en producción se debe que declarar variable de entorno:

```bash
sudo corepack enable
export NODE_ENV=production
```

### Instalar dependencias de JavaScript

#### Usuarios de compilación

Hay bastantes maneras de hacer esto:

1. Es mediante tu mismo usuario y despues cambiando la propiedad de la carpeta en la que instalaste misskey recursivamente. Esta manera es un poco sucia a falta de una mejor palabra en este momento, pero sirve y es la más sencilla.
   
   1. Aquí harías todos los pasos siguentes mediante tu usuario y despues dentro de la carpeta donde instalaste misskey ejecutar `sudo chown -R misskey:misskey .` - El punto dentro de cualquier shell toma el valor de la carpeta en la que estas actualmente, puedes revisar en que carpeta estas con otro comando `pwd` o en vez del punto puedes usar la **ruta** completa para seleccionar en que carpeta quieres efectuar el comando.

2. Hacerlo todo desde el usuario misskey iniciando sesión, desde otro usuario para esto usaríamos el comando `su` que cambia de usuario, conscisamente `sudo su - misskey`.
   
   1. Hacerlo de esta manera es casi lo mismo que la anterior solo que ya no hay que cambiar la poseción de ningun arhivo.
   
   2. Sin embargo tambien si habilitaste que misskey no pueda acceder a ninguna shell este paso será imposible de realizar a menos que cambies la configuración, así que tenlo en cuenta.

3. La tercera forma que esta en medio de ambas es algo sencilla tambien y da los beneficios de ambas sin comprometer seguridad de alguna manera. No podemos acceder a una interfaz de linea de comandos como shell, pero sí es posible ejecutar comandos como ese otro usuario, para esto a todos los comandos que vayamos a ejecutar antes hay que añadirles `sudo -Hu misskey ...`
   
   1. Un truco para realizar los comandos de una manera más sencilla es hacer un alias temporal dentro de la terminal simplemente ejecutando algo como `alias smk="sudo -Hu misskey"` y ahora cada que ejecutes smk dentro de esa sesión el programa correra bajo el usuario misskey.

En todos los casos debemos que ir a la carpeta $HOME de nuestro usuario, en este caso con el comando `cd /sys-users/mk`

----------------------------------------------------------------

Para todas las opciones deberas ejecutar los siguientes comandos:

```bash
git clone --recursive https://github.com/misskey-dev/misskey.git -b master
cd misskey
git submodule update --init
pnpm install --frozen-lockfile
```

----------------------------------------------------------------

Los cuales clonan el repositiorio y los archivos base para compilar misskey y pnpm instala dependencias de javascript de este.

## Configuración

Hay un archivo plantilla para la configuración de misskey, como en muchos paquetes de Debian. Este paso es relativamente sencillo pues no hay que configurar mucho y el archivo se explica a sí mismo. Hay algunas notas importantes como <mark>nunca cambiar el nombre de dominio de un servidor una vez que haya empezado a utilizarlo.</mark>

`cp .config/example.yml .config/default.yml`: Copia `.config/example.yml` y lo renombra a `default.yml`.

Y este último archivo editalo con tu editor de textos favoritos y ya que probablemente estas haciendo esto desde una terminal los comandos que podrias usar son `nano` `vi` `vim` `nvim` etc. dependiendo de que tengas instalado.

### Consideraciones de configuración

En la configuración por defecto todos los archivos que se suban al servidor (drive), sean imágenes, vídeos, pdf, lo que sea, todo se almacenará dentro de la carpeta raíz donde se descargó el repositorio mediante git en una carpeta llamada `files/` y es un poco díficil de administrar sin usar misskey (y no se debería hacer pues también hay una relación existente en la base de datos) pues se guardan con nombres generados por el programa.

## Compilación

Son solo dos comandos, uno va a compilar el software y el segundo va a dejar todo listo para el uso del programa haciendo modificaciones necesarias en la base de datos.

```bash
pnpm run build
pnpm run init
```

Finalmente vamos a iniciar el servidor con:

```bash
NODE_ENV=production pnpm run start
```

# Errores de instalación

Limpiar todo el caché manualmente y reinstalar por completo.

- `pnpm run clean` y `pnpm run clean-all`
- `pnpm rebuild`

# Agradecimientos y recursos de referencia:

[Pleroma Documentation](https://docs-develop.pleroma.social/)

[Misskeyを手動で構築する | Misskey Hub](https://misskey-hub.net/ja/docs/for-admin/install/guides/manual/)

[Ubuntu Misskey Installation Instructions](https://hide.ac/articles/iFwm5HDvH)
[Description of the instance settings and other settings to be set up first on a Misskey instance](https://hide.ac/articles/Y504SIabp)
[Set up squid proxies to protect Misskey](https://hide.ac/articles/MC7WsPDqw)
[Let's back up Misskey's database [OCI Object Storage]](https://hide.ac/articles/E2Ea3cauk)
[Use Oracle Cloud Infrastructure Always Free to set up Misskey instances for free](https://hide.ac/articles/csERs-7SU)
[If your Always Free OCI Arm instance is stopped...](https://hide.ac/articles/qdajqdvj1)
