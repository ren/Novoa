---
title: Integración al sistema
type: docs
prev: docs/misskey
next: http-server
---

Esta parte es un poco complicada porque habla de linux como sistema de nuevo.

No voy a ahondar en detalle porque el archivo ya explica (aunque en inglés) y estoy cansado.

Esta es la unidad del sistema que uso en Novoa, como es algo que escribí de manera personalizada lo he situado dentro de /etc/systemd/system/misskey.service para que no se confundiera con otros archivos.



{{< cards >}}
  {{< card link="/docs/misskey/misskey.service" title="Archivo directo para descarga" icon="folder">}}
{{< /cards >}}

```systemd
[Unit]
Description=Misskey daemon

[Service]
Type=simple
User=misskey
ExecStart=/usr/bin/pnpm start
WorkingDirectory=/other-dir/misskey
Environment="NODE_ENV=production"
TimeoutSec=60
SyslogIdentifier=misskey
Restart=always

; Some security directives.
; Use private /tmp and /var/tmp folders inside a new file system namespace, which are discarded after the process stops.
PrivateTmp=true
; The /home, /root, and /run/user folders can not be accessed by this service anymore. If your misskey user has its home folder in one of the restricted places, or use one of these folders as its working directory, you have to set this to false.
ProtectHome=true
; Sets up a new /dev mount for the process and only adds API pseudo devices like /dev/null, /dev/zero or /dev/random but not physical devices. Disabled by default because it may not work on devices like the Raspberry Pi.
PrivateDevices=true
; Drops the sysadmin capability from the daemon.
CapabilityBoundingSet=~CAP_SYS_ADMIN

# Make the system tree read-only.
ProtectSystem=strict

# Make some paths aviable and readable by the process
ReadWritePaths=/other-dir/misskey

# Allocate a separate /tmp.
PrivateTmp=yes

# Ensure the service can never gain new privileges.
NoNewPrivileges=yes

# Prohibit access to any kind of namespacing.
RestrictNamespaces=yes

# Make cgroup file system hierarchy inaccessible.
ProtectControlGroups=yes

# Deny kernel module loading.
ProtectKernelModules=yes

# Make kernel variables (e.g. /proc/sys) read-only.
ProtectKernelTunables=yes

# Deny hostname changing.
ProtectHostname=yes

# Deny realtime scheduling.
RestrictRealtime=yes

# Deny access to the kernel log ring buffer.
ProtectKernelLogs=yes

# Deny setting the hardware or system clock.
ProtectClock=yes

# Filter dangerous system calls. The following is listed as safe basic
# choice in systemd.exec(5).
SystemCallArchitectures=native

# Deny kernel execution domain changing.
LockPersonality=yes

# Deny memory mappings that are writable and executable.
# Node fucking dies if this is enabled.
#MemoryDenyWriteExecute=yes

[Install]
WantedBy=multi-user.target
```

Después de copiar y pegar esto y hacer respectivas modificaciones como en WorkingDirectory con systemd puedes iniciar el servicio/programa y notarás que utiliza el usuario misskey para ello.

```bash
sudo systemctl daemon-reload
sudo systemctl start misskey
```

Puedes utilizar este comando para habilitar que se inicie automáticamente al encender el servidor.

```bash
sudo systemctl enable misskey
```
