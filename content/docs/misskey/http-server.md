---
title: Servidor HTTP
type: docs
prev: /
next: docs/folder/
---

Usa e instala caddy

Modifica /etc/caddy/Caddyfile

Añade

```
tu.dominio.tld {
    reverse_proxy localhost:$PUERTO_DE_MISSKEY
}
```

Inicia caddy.
