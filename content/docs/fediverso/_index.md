---
title: "Fediverso"
date: 2024-01-13T21:39:10-06:00
draft: false
---

<u>Por cierto en esta sección pondré muchas "páginas" que tenía en [misskey](https://novoa.nagoya/@sarvo/pages/Misskey).</u>

Son varías las bifurcaciones del fediverso, por un lado, esta pleroma y el otro mastodon, mientras en el medio quizás un tanto aislado se encuentra misskey, pues su funcionalidad u orientación del software y proyecto es distinta a la de los otros dos equivalencias de twitter.  

Sé que nada de lo que digo ahora tiene sentido para ti si vienes de fuera, pero lo tendrá al final de este texto.  

El fediverso es una red interconectada de distintos servidores (o instancias) que operan y se comunican mediante un protocolo llamado Activity Pub (sin embargo, también hay otros protocolos) lo que esto permite es que diferentes sitios compartan información de sus usuarios unos con otros, en otras palabras los servidores se comunican y de esta manera también sus usuarios.  

Hay varias orientaciones en el fediverso, distintos tipos de software o servidores que se orientan a distintas tareas, por ejemplo peertube que busca ser una alternativa federada a youtube. Donde estamos ahora es en el lado social, interactivo y variado del fediverso; Mastodon que se creó para ser tan cercano a twitter como le sea posible, Pleroma creado para ser compatible con mastodon, pero al mismo tiempo con sus propias peculiaridades y finalmente Misskey, donde estás ahora leyendo esto si eres usuario de esta instancia, que como te darás cuenta es más bien un software que plantea ser sí mismo con la ocurrencia de que también es federado y se comunica con los otros dos tipos de instancia.  

Puedes identificar cada una de estas por sus iconos en algunas ocasiones, mástodon es azul con un elefante, pleroma por defecto tiene una P naranja con un fondo oscuro y algunos de sus administradores le ponen también una pl a su subdominio, por otro lado, algunos administradores de misskey le ponen una mk a su dirección web (mk.dirección.tld).  

Hay otros recursos que explican en mucho más detalle lo que es el fediverso como los que enlazaré a continuación (aunque están en inglés). Personalmente, pienso que es el futuro y de lo que nunca nos deberíamos haber desviado con las web centralizada de FAGMAN (Facebook, Amazon. Google, Microsoft, Apple, Netflix), somos entonces la alternativa y los pocos que nos hemos salido de ahí por una razón u otra.  

Es así también un oasis o un relicario de la era olvidada del internet de los principios de los 2000s y quizás un poco antes de, de la cultura que también sobrevivió de una manera u otra en 4chan y otros foros aislados, si eres de fuera, tienes que comprender algo crucial, y eso es el anonimato, este internet se rige por avatares, no por tu persona 'real', de esta manera "culturalmente" no es esperado que des información personal, pues es más bien confidencial pues es completamente abierto al público, la idea, de que todo lo que se sube al internet permanece ahí para siempre es completamente verdadera aquí en el fediverso, así que anda con cuidado y más allá de eso, diviértete con tus experiencias aquí, con cualquier icono que hayas seleccionado, quizás y hasta es más verdadero e interesante de lo que imaginas ;)  

## Más información sobre el fediverso:

[https://fediverse.party/](https://fediverse.party/)

https://jointhefedi.com/

[Fediverse Observer | Misskey](https://misskey.fediverse.observer/map)

[GitHub - emilebosch/awesome-fediverse: A curated, collaborative list of awesome Fediverse resources](https://github.com/emilebosch/awesome-fediverse)

## Historia del Fediverso

Gente con mucho mejor prosa que la mía actual ya ha escrito sobre los temas de esta página (sin embargo en inglés) recomiendo mucho su lectura si es de interés:  
GNU Social y el inicio de Mastodon - https://robek.world/featured/what-is-gnu-social-and-is-mastodon-social-a-twitter-clone/  
Otra reflexión antigua sobre el Fediverso por Lain(o lambdalambda, creador de Pleroma) - https://robek.world/futurology/an-admin-what-if-there-was-twitter-without-nazis/


