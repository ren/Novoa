---
title: "Disclaimer"
date: 2024-01-13T22:01:28-06:00
draft: false
---

Robado sin descaro de misskey-hub, pero sigue siendo bastante válido para esta instancia.

## Consideraciones importantes

Hay algunos puntos que deben tenerse en cuenta al utilizar servicios que utilizan Misskey.

Debido a su naturaleza descentralizada, una vez subidos, los datos no están garantizados para ser eliminados de todos los demás servidores, incluso si se borran.(Sin embargo, esto también es cierto para Internet en general).
Incluso si un mensaje se crea como privado, no hay garantía de que los otros servidores lo traten también como privado, así que por favor, ten cuidado cuando publiques información personal o confidencial (aunque esto también ocurre en Internet en general).

Por favor, comprenda estos puntos y disfrute utilizando nuestros servicios.
