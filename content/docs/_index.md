---
title: Documentación de Novoa
date: 2024-01-13T13:59:34-06:00
next: docs/sysadmin
---

Este espacio es para la documentación general del servidor, en cada sección subirñe documentación pertinente a cada servicio.

Iré avanzando lentamente porque me da un poco de pereza y tengo tambien muchas otras cosas que hacer, lo que ves es lo que hay aunque puede que aveces suceda que no actualize la página.

Ya que mis servidores usan Debian y OpenBSD todo lo escrito en la documentación y guías sera en los entornos de alguno de estos sistemas.

Comparto el repositorio el cual esta [aquí↗](https://git.froth.zone/ren/Novoa) por transparencia y por si dejo de subir actualizaciones al servidor.
