---
title: Administración de servidores
type: docs
prev: docs
next: docs/misskey
date: 2024-01-13T13:36:16-06:00
sidebar:
  open: true

---

## Recomendaciones generales

Creo que en primer lugar debes saber y conocer el entorno y/o sistema con el que estas trabajando, en mis servidores he optado por Debian por su versatilidad (en términos de servidores), disponibilidad y estabilidad.

Sobre Debian debes de saber que hay muchos paquetes o software de servidor que puedes instalar directamente desde su administrador de paquetes, no teniendo que compilar el software ni variables de entorno ni nada, lo único que quedaría en manos del administrador sería justamente administrar los servicios, sin necesidad de complicaciones en absoluto.

## Usuarios en linux/UNIX

Estos sistemas fueron diseñados desde un inicio para ser utilizados por varios usuarios de manera simultanea, cada uno con un entorno más o menos aisaldo unos con otros, es decir los programas y datos usados por un usuario no pueden ser usados por otro y viceversa.

Sin embargo también hay un usuario principal llamado super usuario, este usuario también es conocido como root, que es el usuario que puede cambiar todas las configuraciones del sistema e instalar y ejecutar programas a su antojo y sobre otros usuarios, es el usuario que tiene control sobre todo y con el que también se terminan realizando multiples tareas de mantenimiento y administración (como la actualización de software).

Siendo este usuario tan imporante es una buena idea usarlo en la menor medida si no sabes exactamente lo que el comando que estas usando esta realmente realizando, de esta manera, como es un poco imposible verificar y checar todo el código existente en tu sistema lo que se realiza de manera general es hacer un usuario por cada servicio o programa que estes ejecutando (que es también lo que hacen varias distribuciones como Debian).

He de mencionar que hay programas más complejos que quizá requieran el uso de grupos como tareas de respaldo pero eso lo detallaré en otra entrada.

### Privilegios de usuarios

Lo mejor aquí es que ya que son usuarios de alguna u otra manera virtual, creados solo para ejecutar un programa los queremos de una manera aislada y con los privilegios mínimos que se les puedan otorgar, así si el servicio que operamos tiene una falla causará el menor daño en el sistema, el propósito de esto es mitigar las superficie de ataque y este es un patrón es algo usado en todos los programas, como en sus usuarios, sus entornos de ejecución y sus unidades de servicio.
